#!/bin/sh

# GITLAB VARIABLES used:
# - CI_REGISTRY_USER
# - CI_JOB_TOKEN
# - CI_PROJECT_PATH
# - CI_REGISTRY
# CUSTOM VARIABLES:
# - EXISTING_TAG
# - NEW_TAG

# Do not trace command tha may container passwords
set +x
# Exit for unset variables
set -u

# Get a token to authenticate with the registry
curl \
    --silent --show-error --fail \
    --user "${CI_REGISTRY_USER}:${CI_JOB_TOKEN}" \
    -H "Accept: application/json" \
    -d account= \
    -d client_id=docker \
    -d offline_token=true \
    -d service=container_registry \
    -d scope=repository:"${CI_PROJECT_PATH}":push,pull \
    -G "https://gitlab.cern.ch/jwt/auth" | jq .token -r > token.txt
TOKEN=$(cat token.txt)

# Get the manifest for the image we won't to push an extra tag
curl \
    --silent --show-error --fail \
    -X GET \
    -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
    -H "Authorization: Bearer ${TOKEN}" \
    -G "https://${CI_REGISTRY}/v2/${CI_PROJECT_PATH}/manifests/${EXISTING_TAG}" | jq -r . > manifest.json
cat manifest.json

# pysh a new tag for the manifest we fetched before
curl \
    --silent --show-error --fail \
    -X PUT \
    -H "Authorization: Bearer ${TOKEN}" \
    -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
    -H "Content-Type: application/vnd.docker.distribution.manifest.v2+json" \
    -d @manifest.json \
    "https://${CI_REGISTRY}/v2/${CI_PROJECT_PATH}/manifests/${NEW_TAG}"

# Show the new manifest we pushed in the registry
curl \
    --silent --show-error --fail \
    -X GET \
    -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
    -H "Authorization: Bearer ${TOKEN}" \
    -G "https://${CI_REGISTRY}/v2/${CI_PROJECT_PATH}/manifests/${NEW_TAG}" | jq -r . > manifest.json

echo "Pushed tag (manifest):"
cat manifest.json
