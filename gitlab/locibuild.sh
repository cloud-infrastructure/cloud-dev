#!/bin/bash
#
# Performs a build using loci and gitlab variables
#
# GITLAB VARIABLES used:
#   - CI_BUILD_TOKEN
#        token to authenticate with the Gitlab Docker registry
#
#   - CI_REGISTRY_PASSWORD
#        password to log into the Gitlab Docker registry
#
#   - CI_PROJECT_NAME
#        specifies the openstack component to create the image.
#        It maches the name of the repository
#
#   - CI_REPOSITORY_URL
#        location of the git repository
#
#   - CI_COMMIT_SHA
#        reference to the commit to build the image from
#
#   - CI_REGISTRY_IMAGE
#        location of the registry for this particular project
#
git clone https://git.openstack.org/openstack/loci.git /tmp/loci && cd /tmp/loci
buildah --storage-driver vfs bud --isolation chroot --build-arg FROM=centos:7 --build-arg PROJECT=$CI_PROJECT_NAME --build-arg WHEELS=loci/requirements:master-centos --build-arg PROJECT_REPO=$CI_REPOSITORY_URL --build-arg PROJECT_REF=$CI_COMMIT_SHA -t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA .
buildah --storage-driver vfs push --creds gitlab-ci-token:$CI_BUILD_TOKEN $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA docker://$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
buildah --storage-driver vfs rmi $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
