#!/bin/bash
#
# Performs a build using loci and gitlab variables
#
# GITLAB VARIABLES used:
#   - CI_REGISTRY_USER
#        user required to log into the Gitlab Docker registry
#
#   - CI_REGISTRY_PASSWORD
#        password to log into the Gitlab Docker registry
#
#   - CI_REGISTRY
#        location of the gitlab registry our case it is resolved to
#        gitlab-registry.cern.ch
#
#   - CI_COMMIT_SHA
#        reference to the commit to build the image from
#
#   - CI_COMMIT_TAG
#        tag created that will be applied to the image
#
#   - CI_REGISTRY_IMAGE
#        location of the registry for this particular project
#
buildah --storage-driver vfs pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
DIGEST=$(skopeo inspect "docker://${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}" | jq .Digest -r |  grep -oE '[0-9a-f]{64}')

curl --silent \
  --user svcbuild:${SVCBUILD_PASSWORD} \
  -H "Accept: application/json" \
  -d account= \
  -d client_id=docker \
  -d offline_token=true \
  -d service=container_registry \
  -d scope=repository:${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:*
  -G https://gitlab.cern.ch/jwt/auth | jq .token -r > token.txt
TOKEN=$(cat token.txt)

curl --silent \
  -X DELETE \
  -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
  -H "Authorization: Bearer ${TOKEN}" \
  -G https://gitlab-registry.cern.ch/v2/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/manifests/sha256:${DIGEST}

buildah --storage-driver vfs tag $REGISTRY_IMAGE:$CI_COMMIT_SHA $REGISTRY_IMAGE:$TAG
buildah --storage-driver vfs push --creds gitlab-ci-token:$CI_BUILD_TOKEN $REGISTRY_IMAGE:$TAG docker://$REGISTRY_IMAGE:$TAG
