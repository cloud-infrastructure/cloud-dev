#!/bin/bash
#
# Tags a package after being build by koji (version-release).
#
# PARAMS:
#   - CI_PROJECT_DIR
#        the location of the code (including the spec file). if using gitlab
#        this is predefined by the runner
#   - KOJI_TAG
#        the koji tag to tag against. Normally based on KOJI_TARGET variable
#   - DIST
#        the package distribution (.el7 | .el7.cern for example) defaults to .el7
#   - SVCBUILD_PASSWORD
#        the password of the svcbuild user. if using gitlab define this in Settings/Variables
#
cd $CI_PROJECT_DIR
if [ -z "$DIST" ]
then
    export DIST=".el7"
fi
sed -i "s/%dist .el7.cern/%dist ${DIST}/g" /etc/rpm/macros.dist
export SPEC=$(ls *spec)
export PKG=$(rpm -q --specfile $SPEC --queryformat "[%{name}-%{version}-%{release}\n]" | head -n1)
echo $SVCBUILD_PASSWORD | kinit svcbuild@CERN.CH
koji tag-build $KOJI_TAG $PKG
